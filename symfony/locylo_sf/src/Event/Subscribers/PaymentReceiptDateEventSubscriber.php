<?php

namespace App\Event\Subscribers;

use App\Event\PaymentReceiptDateEvent;
use App\Services\Receipt\ReceiptManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class PaymentReceiptDateEventSubscriber implements EventSubscriberInterface
{
    private ReceiptManager $receiptManager;

    public function __construct(ReceiptManager $receiptManager)
    {
        $this->receiptManager = $receiptManager;
    }

    /**
     * {@inheritDoc}
     *
     * @return array[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            PaymentReceiptDateEvent::NAME => 'handleReceiptDate',
        ];
    }

    public function handleReceiptDate(PaymentReceiptDateEvent $event): void
    {
        $this->receiptManager->sendReceipt($event->getPayment());
    }
}
