<?php

namespace App\Event\Subscribers;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Model\FlatPictureModel;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class FlatPictureModelPostSubscriber implements EventSubscriberInterface
{
    private NormalizerInterface $normalizer;

    public function __construct(NormalizerInterface $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public static function getSubscribedEvents(): array
    {
        return [
                KernelEvents::VIEW => ['fillResponseWithContentUrl', EventPriorities::PRE_SERIALIZE],
        ];
    }

    public function fillResponseWithContentUrl(ViewEvent $event): void
    {
        if ('api_flat_picture_models_post_collection' !== $event->getRequest()->get('_route')) {
            return;
        }
        /** @var FlatPictureModel $flatPictureModel */
        $flatPictureModel = $event->getControllerResult();

        $event
            ->setResponse(
                new JsonResponse(
                    $this->normalizer->normalize($flatPictureModel, null, ['groups' => ['flat:pictures']]),
                    Response::HTTP_CREATED
                )
            );
    }
}
