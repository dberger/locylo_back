<?php

namespace App\Event;

use App\Entity\TenantPayment;
use Symfony\Contracts\EventDispatcher\Event;

final class PaymentReceiptDateEvent extends Event
{
    public const NAME = 'payment.receipt.send';

    protected TenantPayment $payment;

    public function __construct(TenantPayment $payment)
    {
        $this->payment = $payment;
    }

    public function getPayment(): TenantPayment
    {
        return $this->payment;
    }
}
