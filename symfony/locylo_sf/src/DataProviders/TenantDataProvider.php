<?php

namespace App\DataProviders;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Model\TenantModel;
use App\Services\Tenant\TenantSearchProvider;

final class TenantDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface, ItemDataProviderInterface
{
    private TenantSearchProvider $tenantSearchProvider;

    /**
     * TenantDataProvider constructor.
     */
    public function __construct(TenantSearchProvider $tenantSearchProvider)
    {
        $this->tenantSearchProvider = $tenantSearchProvider;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return TenantModel::class === $resourceClass;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        $tenants = $this->tenantSearchProvider->findAll();

        foreach ($tenants as $tenant) {
            yield (new TenantModel())->buildFromEntity($tenant);
        }
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?TenantModel
    {
        $tenant = $this->tenantSearchProvider->find($id);

        if (!$tenant) {
            return null;
        }

        return (new TenantModel())->buildFromEntity($tenant);
    }
}
