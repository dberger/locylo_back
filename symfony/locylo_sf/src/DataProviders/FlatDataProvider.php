<?php

namespace App\DataProviders;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Model\FlatModel;
use App\Services\Flat\FlatSearchProvider;
use App\Services\Payments\FlatPaymentsBookManager;

final class FlatDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface, ItemDataProviderInterface
{
    private FlatSearchProvider $flatSearchProvider;
    private FlatPaymentsBookManager $flatPaymentsBookManager;

    /**
     * FlatCollectionDataProvider constructor.
     */
    public function __construct(FlatSearchProvider $flatSearchProvider, FlatPaymentsBookManager $flatPaymentsBookManager)
    {
        $this->flatSearchProvider = $flatSearchProvider;
        $this->flatPaymentsBookManager = $flatPaymentsBookManager;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return FlatModel::class === $resourceClass;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        $flats = $this->flatSearchProvider->findAll();

        foreach ($flats as $flat) {
            yield (new FlatModel())->buildFromEntity($flat);
        }
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?FlatModel
    {
        $flat = $this->flatSearchProvider->find($id);

        if (!$flat) {
            return null;
        }

        return $this->flatPaymentsBookManager->addMissingPaymentsInModel($flat, (new FlatModel())->buildFromEntity($flat));
    }
}
