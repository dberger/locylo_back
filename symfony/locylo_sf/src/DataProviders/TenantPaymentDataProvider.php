<?php

namespace App\DataProviders;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Model\TenantPaymentModel;
use App\Services\Flat\TenantPaymentSearchProvider;

final class TenantPaymentDataProvider implements RestrictedDataProviderInterface, ItemDataProviderInterface
{
    private TenantPaymentSearchProvider $tenantPaymentSearchProvider;

    /**
     * TenantPaymentCollectionDataProvider constructor.
     */
    public function __construct(TenantPaymentSearchProvider $tenantPaymentSearchProvider)
    {
        $this->tenantPaymentSearchProvider = $tenantPaymentSearchProvider;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return TenantPaymentModel::class === $resourceClass;
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?TenantPaymentModel
    {
        $tenantPayment = $this->tenantPaymentSearchProvider->find($id);

        if (!$tenantPayment) {
            return null;
        }

        return (new TenantPaymentModel())->buildFromEntity($tenantPayment);
    }
}
