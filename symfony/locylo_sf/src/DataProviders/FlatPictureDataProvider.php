<?php

namespace App\DataProviders;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\FlatPicture;
use App\Model\FlatPictureModel;
use App\Repository\FlatPictureRepository;

final class FlatPictureDataProvider implements RestrictedDataProviderInterface, ItemDataProviderInterface
{
    private FlatPictureRepository $flatPictureRepository;

    /**
     * FlatCollectionDataProvider constructor.
     */
    public function __construct(FlatPictureRepository $flatPictureRepository)
    {
        $this->flatPictureRepository = $flatPictureRepository;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return FlatPictureModel::class === $resourceClass;
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?FlatPictureModel
    {
        /** @var FlatPicture $flatPicture */
        $flatPicture = $this->flatPictureRepository->find($id);

        if (!$flatPicture) {
            return null;
        }

        return (new FlatPictureModel())->buildFromEntity($flatPicture);
    }
}
