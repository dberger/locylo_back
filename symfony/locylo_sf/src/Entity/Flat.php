<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FlatRepository")
 * @ORM\Table(name="flat",
 *     options={"comment":"Table of Flat"}
 * )
 */
class Flat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"comment":"Flat id"})
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Name of the flat"})
     */
    #[Assert\NotBlank]
    private ?string $name;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"comment":"Flat Price"}, nullable=true)
     */
    private ?float $flatPrice;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"comment":"Notary fees"}, nullable=true)
     */
    private ?float $notaryFees;

    /**
     * @ORM\Column(type="datetime", options={"comment":"Purchase Date"}, nullable=true)
     */
    private ?DateTimeInterface $purchaseDate;

    /**
     * @ORM\OneToOne(targetEntity="Address", mappedBy="flat", cascade={"persist", "remove"})
     */
    private ?Address $address;

    /**
     * @ORM\OneToOne(targetEntity="RentalDetails", mappedBy="flat", cascade={"persist", "remove"})
     */
    private ?RentalDetails $rentalDetails;

    /**
     * @ORM\OneToMany(targetEntity="Insurance", mappedBy="flat", cascade={"persist", "remove"})
     */
    private Collection $insurances;

    /**
     * @ORM\OneToMany(targetEntity="TenantFlat", mappedBy="flat", mappedBy="flat")
     */
    private Collection $tenants;

    /**
     * @ORM\OneToMany(targetEntity="TenantPayment", mappedBy="flat")
     */
    private Collection $payments;

    /**
     * @ORM\OneToMany(targetEntity="FlatPicture", mappedBy="flat")
     */
    private Collection $pictures;

    private ?Tenant $tmpTenant = null;

    private ?TenantPayment $tmpLastPayment = null;

    private ?TenantFlat $tmpTenantFlat = null;

    public function __construct()
    {
        $this->insurances = new ArrayCollection();
        $this->tenants = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->pictures = new ArrayCollection();
    }

    public function getRentPrice(): ?float
    {
        return $this->getRentalDetails() instanceof RentalDetails ?
            $this->getRentalDetails()->getTotalPrice()
            : null;
    }

    public function getInsuranceExpirationDate(): ?DateTimeInterface
    {
        foreach ($this->getInsurances() as $insurance) {
            if ($insurance->getActive()) {
                return $insurance->getExpiredAt();
            }
        }

        return null;
    }

    public function getLastRentPaidOn(): ?DateTimeInterface
    {
        if ($this->tmpLastPayment) {
            return $this->tmpLastPayment->getPaymentDate();
        }

        $payment = $this->getLastPayment();

        return $payment ? $payment->getPaymentDate() : null;
    }

    public function getLastPayment(): ?TenantPayment
    {
        if ($this->tmpLastPayment) {
            return $this->tmpLastPayment;
        }

        $maxDate = null;

        foreach ($this->getPayments() as $payment) {
            if (!$maxDate) {
                $maxDate = $payment->getPaymentDate();
            }
            if ($payment->getPaymentDate() >= $maxDate) {
                $maxDate = $payment->getPaymentDate();
                $this->tmpLastPayment = $payment;
            }
        }

        return $this->tmpLastPayment;
    }

    public function getLastCreatedPayment(): ?TenantPayment
    {
        $maxPayment = null;

        foreach ($this->getPayments() as $payment) {
            if (!$maxPayment) {
                $maxPayment = $payment;
            }
            if ($payment->getCreatedAt() >= $maxPayment->getCreatedAt()) {
                $maxPayment = $payment;
            }
        }

        return $maxPayment;
    }

    public function getRentedSince(): ?DateTimeInterface
    {
        $now = new DateTime();
        foreach ($this->getTenants() as $tenantFlat) {
            if ($tenantFlat->getExpiredAt() > $now) {
                $this->tmpTenant = $tenantFlat->getTenant();

                return $tenantFlat->getStartAt();
            }
        }

        return null;
    }

    public function getActiveTenant(): ?Tenant
    {
        if ($this->tmpTenant instanceof Tenant) {
            return $this->tmpTenant;
        }

        if ($this->tmpTenantFlat instanceof TenantFlat) {
            return $this->tmpTenantFlat->getTenant();
        }

        $now = new DateTime();

        foreach ($this->getTenants() as $tenantFlat) {
            if ($tenantFlat->getExpiredAt() > $now) {
                $this->tmpTenant = $tenantFlat->getTenant();
                $this->tmpTenantFlat = $tenantFlat;

                return $tenantFlat->getTenant();
            }
        }

        return null;
    }

    public function getActiveTenantFlat(): ?TenantFlat
    {
        if ($this->tmpTenantFlat instanceof TenantFlat) {
            return $this->tmpTenantFlat;
        }

        $now = new DateTime();

        foreach ($this->getTenants() as $tenantFlat) {
            if ($tenantFlat->getExpiredAt() > $now) {
                $this->tmpTenantFlat = $tenantFlat;

                return $tenantFlat;
            }
        }

        return null;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): Flat
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): Flat
    {
        $this->name = $name;

        return $this;
    }

    public function getFlatPrice(): ?float
    {
        return $this->flatPrice;
    }

    public function setFlatPrice(?float $flatPrice): Flat
    {
        $this->flatPrice = $flatPrice;

        return $this;
    }

    public function getNotaryFees(): ?float
    {
        return $this->notaryFees;
    }

    public function setNotaryFees(?float $notaryFees): Flat
    {
        $this->notaryFees = $notaryFees;

        return $this;
    }

    public function getPurchaseDate(): ?DateTimeInterface
    {
        return $this->purchaseDate;
    }

    public function setPurchaseDate(?DateTimeInterface $purchaseDate): Flat
    {
        $this->purchaseDate = $purchaseDate;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): Flat
    {
        $this->address = $address;

        return $this;
    }

    public function getRentalDetails(): ?RentalDetails
    {
        return $this->rentalDetails;
    }

    public function setRentalDetails(?RentalDetails $rentalDetails): Flat
    {
        $this->rentalDetails = $rentalDetails;

        return $this;
    }

    /**
     * @return Collection|Insurance[]
     */
    public function getInsurances(): ArrayCollection|Collection
    {
        return $this->insurances;
    }

    public function setInsurances(ArrayCollection|Collection $insurances): Flat
    {
        $this->insurances = $insurances;

        return $this;
    }

    /**
     * @return TenantFlat[]|Collection
     */
    public function getTenants(): ArrayCollection|Collection
    {
        return $this->tenants;
    }

    public function setTenants(ArrayCollection|Collection $tenants): Flat
    {
        $this->tenants = $tenants;

        return $this;
    }

    /**
     * @return Collection|TenantPayment[]
     */
    public function getPayments(): ArrayCollection|Collection
    {
        return $this->payments;
    }

    public function setPayments(ArrayCollection|Collection $payments): Flat
    {
        $this->payments = $payments;

        return $this;
    }

    public function getPictures(): ArrayCollection|Collection
    {
        return $this->pictures;
    }

    public function setPictures(ArrayCollection|Collection $pictures): Flat
    {
        $this->pictures = $pictures;

        return $this;
    }
}
