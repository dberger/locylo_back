<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="rental_details",
 *     options={"comment":"Table of Rental details"}
 * )
 */
class RentalDetails
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"comment":"Rental details id"})
     */
    private ?int $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"comment":"Price without charges"})
     */
    #[Assert\NotBlank]
    private ?float $price;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"comment":"Charges amount"})
     */
    #[Assert\NotBlank]
    private ?float $chargesAmount;

    /**
     * @ORM\Column(type="datetime", options={"comment":"Creation At"})
     */
    private ?DateTimeInterface $createdAt;

    /**
     * @ORM\Column(type="datetime", options={"comment":"Updated At"})
     */
    private ?DateTimeInterface $updatedAt;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Flat", inversedBy="rentalDetails")
     */
    private ?Flat $flat;

    public function __construct()
    {
        $this->createdAt = new DateTime();
        if (empty($this->updatedAt)) {
            $this->setUpdatedAt(new DateTime());
        }
    }

    public function getTotalPrice(): ?float
    {
        return $this->getPrice() + $this->getChargesAmount();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): RentalDetails
    {
        $this->id = $id;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): RentalDetails
    {
        $this->price = $price;

        return $this;
    }

    public function getChargesAmount(): ?float
    {
        return $this->chargesAmount;
    }

    public function setChargesAmount(?float $chargesAmount): RentalDetails
    {
        $this->chargesAmount = $chargesAmount;

        return $this;
    }

    public function getCreatedAt(): DateTimeInterface|null
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface|null $createdAt): RentalDetails
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): RentalDetails
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getFlat(): ?Flat
    {
        return $this->flat;
    }

    public function setFlat(?Flat $flat): RentalDetails
    {
        $this->flat = $flat;

        return $this;
    }
}
