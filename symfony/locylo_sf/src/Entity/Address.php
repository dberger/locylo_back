<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="address",
 *     options={"comment":"Table of Addresses"}
 * )
 */
class Address
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"comment":"Address id"})
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Address (street number/name)"})
     */
    #[Assert\NotBlank]
    private ?string $address;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Zip code"})
     */
    #[Assert\NotBlank]
    private ?string $zip;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"City name"})
     */
    #[Assert\NotBlank]
    private ?string $city;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Latitude"}, nullable=true)
     */
    private ?string $lat;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Longitude"}, nullable=true)
     */
    private ?string $lng;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Flat", inversedBy="address")
     */
    private ?Flat $flat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): Address
    {
        $this->id = $id;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): Address
    {
        $this->address = $address;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): Address
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): Address
    {
        $this->city = $city;

        return $this;
    }

    public function getLat(): ?string
    {
        return $this->lat;
    }

    public function setLat(?string $lat): Address
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLng(): ?string
    {
        return $this->lng;
    }

    public function setLng(?string $lng): Address
    {
        $this->lng = $lng;

        return $this;
    }

    public function getFlat(): ?Flat
    {
        return $this->flat;
    }

    public function setFlat(?Flat $flat): Address
    {
        $this->flat = $flat;

        return $this;
    }
}
