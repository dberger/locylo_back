<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\CreateFlatPictureAction;
use App\Model\FlatPictureModel;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @Vich\Uploadable
 */
#[ApiResource(
    collectionOperations: [
        'get',
        'post' => [
            'controller' => CreateFlatPictureAction::class,
            'deserialize' => false,
            'validation_groups' => ['Default', 'flat_picture_create'],
            'openapi_context' => [
                'requestBody' => [
                    'content' => [
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'file' => [
                                        'type' => 'string',
                                        'format' => 'binary',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    iri: 'http://schema.org/FlatPicture',
    itemOperations: ['get'],
    normalizationContext: ['groups' => ['flat_picture_create:read']]
)]
class FlatPicture
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @ORM\Id
     */
    private ?int $id = null;

    #[ApiProperty(iri: 'http://schema.org/contentUrl')]
    #[Groups(['media_object:read', 'flat:pictures'])]
    public ?string $contentUrl = null;

    /**
     * @Vich\UploadableField(mapping="flatpicture", fileNameProperty="filePath")
     */
    #[Assert\NotNull(groups: ['flat_picture_create'])]
    public ?File $file = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    #[Groups(['media_object:read', 'flat:pictures'])]
    private ?DateTime $updatedAt = null;

    /**
     * @ORM\Column(nullable=true)
     */
    public ?string $filePath = null;

    /**
     * @ORM\ManyToOne(targetEntity="Flat", inversedBy="pictures")
     * @ORM\JoinColumn(nullable=false)
     */
    #[Assert\NotBlank]
    private ?Flat $flat;

    public function createFromModel(FlatPictureModel $flatPictureModel): FlatPicture
    {
        return $this
            ->setId($flatPictureModel->getId())
            ->setFile($flatPictureModel->getFile())
            ->setFilePath($flatPictureModel->getFilePath())
            ->setContentUrl($flatPictureModel->getContentUrl())
            ;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): FlatPicture
    {
        $this->id = $id;

        return $this;
    }

    public function getContentUrl(): ?string
    {
        return $this->contentUrl;
    }

    public function setContentUrl(?string $contentUrl): FlatPicture
    {
        $this->contentUrl = $contentUrl;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): FlatPicture
    {
        $this->file = $file;
        $this->updatedAt = new DateTime();

        return $this;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function setFilePath(?string $filePath): FlatPicture
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function getFlat(): ?Flat
    {
        return $this->flat;
    }

    public function setFlat(?Flat $flat): FlatPicture
    {
        $this->flat = $flat;

        return $this;
    }

    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }
}
