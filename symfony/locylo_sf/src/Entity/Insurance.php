<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="insurance",
 *     options={"comment":"Table of Insurances"}
 * )
 */
class Insurance
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"comment":"Insurance id"})
     */
    private ?int $id;

    /**
     * @ORM\Column(type="datetime", options={"comment":"Insurance start at"})
     */
    #[Assert\NotBlank]
    private ?DateTimeInterface $startAt;

    /**
     * @ORM\Column(type="datetime", options={"comment":"Insurance end at"})
     */
    #[Assert\NotBlank]
    private ?DateTimeInterface $expiredAt;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Company name"})
     */
    #[Assert\NotBlank]
    private ?string $company;

    /**
     * @ORM\Column(type="boolean", options={"comment":"Is active"})
     */
    #[Assert\NotBlank]
    private ?bool $active = false;

    /**
     * @ORM\ManyToOne(targetEntity="Flat", inversedBy="insurances")
     * @ORM\JoinColumn(nullable=false)
     */
    #[Assert\NotBlank]
    private ?Flat $flat;

    /**
     * @ORM\ManyToOne(targetEntity="Tenant", inversedBy="insurances")
     * @ORM\JoinColumn(nullable=false)
     */
    #[Assert\NotBlank]
    private ?Tenant $tenant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): Insurance
    {
        $this->id = $id;

        return $this;
    }

    public function getStartAt(): ?DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(?DateTimeInterface $startAt): Insurance
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getExpiredAt(): ?DateTimeInterface
    {
        return $this->expiredAt;
    }

    public function setExpiredAt(?DateTimeInterface $expiredAt): Insurance
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): Insurance
    {
        $this->company = $company;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): Insurance
    {
        $this->active = $active;

        return $this;
    }

    public function getFlat(): ?Flat
    {
        return $this->flat;
    }

    public function setFlat(?Flat $flat): Insurance
    {
        $this->flat = $flat;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): Insurance
    {
        $this->tenant = $tenant;

        return $this;
    }
}
