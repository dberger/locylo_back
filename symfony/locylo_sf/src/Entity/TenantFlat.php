<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="tenant_flat",
 *     options={"comment":"Tenant-Flat join table"}
 * )
 */
class TenantFlat
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Flat", inversedBy="tenants")
     * @ORM\JoinColumn(nullable=false)
     */
    #[Assert\NotBlank]
    private ?Flat $flat;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Tenant")
     * @ORM\JoinColumn(nullable=false)
     */
    #[Assert\NotBlank]
    private ?Tenant $tenant;

    /**
     * @ORM\Column(type="datetime", options={"comment":"Start of the lease"})
     */
    #[Assert\NotBlank]
    private ?DateTimeInterface $startAt;

    /**
     * @ORM\Column(type="datetime", options={"comment":"End of the lease"})
     */
    #[Assert\NotBlank]
    private ?DateTimeInterface $expiredAt;

    public function getStartAt(): ?DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(?DateTimeInterface $startAt): TenantFlat
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getExpiredAt(): ?DateTimeInterface
    {
        return $this->expiredAt;
    }

    public function setExpiredAt(?DateTimeInterface $expiredAt): TenantFlat
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    public function getFlat(): ?Flat
    {
        return $this->flat;
    }

    public function setFlat(?Flat $flat): TenantFlat
    {
        $this->flat = $flat;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): TenantFlat
    {
        $this->tenant = $tenant;

        return $this;
    }
}
