<?php

namespace App\Entity;

use App\Model\TenantModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="tenant",
 *     options={"comment":"Table of Tenants"}
 * )
 */
class Tenant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"comment":"Tenant id"})
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Last name"})
     */
    #[Assert\NotBlank]
    private ?string $lastname;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"First name"}, nullable=true)
     */
    private ?string $firstname;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Phone number"}, nullable=true)
     */
    private ?string $phone;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Mail address"}, nullable=true)
     */
    private ?string $mail;

    /**
     * @ORM\OneToMany(targetEntity="Insurance", mappedBy="tenant")
     */
    private Collection $insurances;

    public function createFromModel(TenantModel $tenantModel): Tenant
    {
        return $this
            ->setFirstname($tenantModel->getFirstname())
            ->setLastname($tenantModel->getLastname())
            ->setMail($tenantModel->getMail())
            ->setPhone($tenantModel->getPhone());
    }

    public function __construct()
    {
        $this->insurances = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): Tenant
    {
        $this->id = $id;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): Tenant
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): Tenant
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): Tenant
    {
        $this->phone = $phone;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(?string $mail): Tenant
    {
        $this->mail = $mail;

        return $this;
    }

    public function getInsurances(): ArrayCollection|Collection
    {
        return $this->insurances;
    }

    public function setInsurances(ArrayCollection|Collection $insurances): Tenant
    {
        $this->insurances = $insurances;

        return $this;
    }
}
