<?php

namespace App\Entity;

use App\Model\TenantPaymentModel;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="tenant_payment",
 *     options={"comment":"Payments table"}
 * )
 */
class TenantPayment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"comment":"Payment id"})
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity="Flat", inversedBy="payments")
     * @ORM\JoinColumn(nullable=false)
     */
    #[Assert\NotBlank]
    private ?Flat $flat;

    /**
     * @ORM\ManyToOne(targetEntity="Tenant")
     * @ORM\JoinColumn(nullable=false)
     */
    #[Assert\NotBlank]
    private ?Tenant $tenant;

    /**
     * @ORM\Column(type="datetime", options={"comment":"Payment received At"}, nullable=true)
     */
    #[Assert\NotBlank]
    private ?DateTimeInterface $paymentDate;

    /**
     * @ORM\Column(type="datetime", options={"comment":"Receipt send at"}, nullable=true)
     */
    #[Assert\NotBlank]
    private ?DateTimeInterface $receiptSendAt;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"comment":"Amount Paid"})
     */
    #[Assert\NotBlank]
    private ?float $amountPaid;

    /**
     * @ORM\Column(type="datetime", options={"comment":"Debt Date"})
     */
    #[Assert\NotBlank]
    private ?DateTimeInterface $debtDate;

    /**
     * @ORM\Column(type="datetime", options={"comment":"Created At"})
     */
    #[Assert\NotBlank]
    private ?DateTimeInterface $createdAt;

    /**
     * @ORM\Column(type="datetime", options={"comment":"Updated At"})
     */
    #[Assert\NotBlank]
    private ?DateTimeInterface $updatedAt;

    public function __construct()
    {
        $this->createdAt = new DateTime();
        if (empty($this->updatedAt)) {
            $this->setUpdatedAt(new DateTime());
        }
    }

    public function createFromModel(TenantPaymentModel $tenantPaymentModel): TenantPayment
    {
        return $this
            ->setId($tenantPaymentModel->getId())
            ->setAmountPaid($tenantPaymentModel->getAmountPaid())
            ->setPaymentDate($tenantPaymentModel->getPaymentDate())
            ->setReceiptSendAt($tenantPaymentModel->getReceiptSendAt())
            ->setDebtDate($tenantPaymentModel->getDebtDate())
            ;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): TenantPayment
    {
        $this->id = $id;

        return $this;
    }

    public function getPaymentDate(): ?DateTimeInterface
    {
        return $this->paymentDate;
    }

    public function setPaymentDate(?DateTimeInterface $paymentDate): TenantPayment
    {
        $this->paymentDate = $paymentDate;

        return $this;
    }

    public function getReceiptSendAt(): ?DateTimeInterface
    {
        return $this->receiptSendAt;
    }

    public function setReceiptSendAt(?DateTimeInterface $receiptSendAt): TenantPayment
    {
        $this->receiptSendAt = $receiptSendAt;

        return $this;
    }

    public function getAmountPaid(): ?float
    {
        return $this->amountPaid;
    }

    public function setAmountPaid(?float $amountPaid): TenantPayment
    {
        $this->amountPaid = $amountPaid;

        return $this;
    }

    public function getFlat(): ?Flat
    {
        return $this->flat;
    }

    public function setFlat(?Flat $flat): TenantPayment
    {
        $this->flat = $flat;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): TenantPayment
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getDebtDate(): ?DateTimeInterface
    {
        return $this->debtDate;
    }

    public function setDebtDate(?DateTimeInterface $debtDate): TenantPayment
    {
        $this->debtDate = $debtDate;

        return $this;
    }

    public function getCreatedAt(): DateTimeInterface|DateTime|null
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface|DateTime|null $createdAt): TenantPayment
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): TenantPayment
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
