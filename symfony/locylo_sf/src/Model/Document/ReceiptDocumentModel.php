<?php

namespace App\Model\Document;

final class ReceiptDocumentModel
{
    public const MODEL_PATH = '/data/models/RECEIPT_MODEL.docx';
    public const OUTPUT_PATH = '/data/receipts/{flatId}';

    public const VALUES_TO_REPLACE = [
        'SENDER',
        'SENDER_ADDR',
        'DATE_SEND',
        'PERIOD',
        'TENANT',
        'TENANT_ADDR',
        'FLAT_PRICE',
        'FLAT_CHARGES',
        'FLAT_TOTAL_PRICE',
        'PAYMENT_DATE',
        'FLAT_ADDR',
        'AMOUNT_PAID',
    ];
}
