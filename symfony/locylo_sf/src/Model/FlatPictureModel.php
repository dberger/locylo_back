<?php

namespace App\Model;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\CreateFlatPictureAction;
use App\Entity\FlatPicture;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable
 */
#[ApiResource(
    collectionOperations: [
    'post' => [
        'validation_groups' => ['Default', 'flat_picture_create'],
        'controller' => CreateFlatPictureAction::class,
        'deserialize' => false,
        'normalizationContext' => ['groups' => ['flat:pictures']],
        'denormalizationContext' => ['groups' => ['flat:pictures']],
        'openapi_context' => [
            'requestBody' => [
                'content' => [
                    'multipart/form-data' => [
                        'schema' => [
                            'type' => 'object',
                            'properties' => [
                                'file' => [
                                    'type' => 'string',
                                    'format' => 'binary',
                                ],
                                'flatId' => [
                                    'type' => 'string',
                                    'format' => 'string',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
],
    iri: 'http://schema.org/FlatPictureModel',
    itemOperations: [
    'get' => [
        'normalizationContext' => ['groups' => ['flat:pictures']],
        'denormalizationContext' => ['groups' => ['flat:pictures']],
    ],
    'delete' => [],
],
    denormalizationContext: ['groups' => ['flat_picture_create:add']],
    normalizationContext: ['groups' => ['flat_picture_create:read']]
)]
class FlatPictureModel implements BaseModelInterface
{
    #[Groups(['flat:pictures', 'flat:collection'])]
    private ?int $id = null;

    #[ApiProperty(iri: 'http://schema.org/contentUrl')]
    #[Groups(['flat_picture_create:read', 'flat:pictures', 'flat:collection'])]
    public ?string $contentUrl = null;

    /**
     * @Vich\UploadableField(mapping="flatpicture", fileNameProperty="filePath")
     */
    #[Assert\NotNull(groups: ['flat_picture_create'])]
    #[Groups(['flat_picture_create:add'])]
    public ?File $file = null;

    /**
     * @ORM\Column(nullable=true)
     */
    public ?string $filePath = null;

    #[Assert\NotBlank(groups: ['flat_picture_create'])]
    #[Groups(['flat_picture_create:add', 'flat:pictures'])]
    private string $flatId;

    public function buildFromEntity(object $flatPicture): FlatPictureModel
    {
        return $this
            ->setFlatId($flatPicture->getFlat()->getId())
            ->setContentUrl($flatPicture->getContentUrl())
            ->setFilePath($flatPicture->getFilePath())
            ->setId($flatPicture->getId())
            ->setFile($flatPicture->getFile());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): FlatPictureModel
    {
        $this->id = $id;

        return $this;
    }

    public function getContentUrl(): ?string
    {
        return $this->contentUrl;
    }

    public function setContentUrl(?string $contentUrl): FlatPictureModel
    {
        $this->contentUrl = $contentUrl;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): FlatPictureModel
    {
        $this->file = $file;

        return $this;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function setFilePath(?string $filePath): FlatPictureModel
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function getFlatId(): string
    {
        return $this->flatId;
    }

    public function setFlatId(string $flatId): FlatPictureModel
    {
        $this->flatId = $flatId;

        return $this;
    }
}
