<?php

namespace App\Model;

use App\Entity\RentalDetails;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class RentalDetailsModel.
 */
class RentalDetailsModel implements BaseModelInterface
{
    /**
     * @Groups({"flat:item"})
     */
    private ?int $id;

    /**
     * @Groups({"flat:item"})
     */
    #[Assert\NotBlank]
    private ?float $price;

    /**
     * @Groups({"flat:item"})
     */
    #[Assert\NotBlank]
    private ?float $chargesAmount;

    /**
     * @param RentalDetails $rentalDetails
     *
     * @return $this
     */
    public function buildFromEntity(object $rentalDetails): self
    {
        return $this
            ->setId($rentalDetails->getId())
            ->setPrice($rentalDetails->getPrice())
            ->setChargesAmount($rentalDetails->getChargesAmount());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): RentalDetailsModel
    {
        $this->id = $id;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): RentalDetailsModel
    {
        $this->price = $price;

        return $this;
    }

    public function getChargesAmount(): ?float
    {
        return $this->chargesAmount;
    }

    public function setChargesAmount(?float $chargesAmount): RentalDetailsModel
    {
        $this->chargesAmount = $chargesAmount;

        return $this;
    }
}
