<?php

namespace App\Model;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Flat;
use App\Entity\FlatPicture;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class FlatModel.
 */
#[ApiResource(
    collectionOperations: ['get' => ['normalization_context' => ['groups' => ['flat:collection']]]],
    itemOperations: [
    'get' => ['normalization_context' => ['groups' => ['flat:collection', 'flat:item', 'flat:pictures']]],
    'get_pictures' => [
        'normalization_context' => ['groups' => ['flat:pictures']],
        'path' => '/flat_model/{id}/pictures',
        'method' => 'GET',
    ],
],
)]
class FlatModel implements BaseModelInterface
{
    /**
     * @Groups({"flat:collection", "flat:pictures"})
     */
    private ?int $id;

    /**
     * @var string|null
     * @Groups({"flat:collection"})
     */
    #[Assert\NotBlank]
    private ?string $name;

    /**
     * @Groups({"flat:item"})
     */
    private ?float $flatPrice;

    /**
     * @Groups({"flat:item"})
     */
    private ?float $notaryFees;

    /**
     * @Groups({"flat:collection"})
     */
    private ?DateTimeInterface $rentedSince;

    /**
     * @Groups({"flat:collection"})
     */
    private ?DateTimeInterface $lastRentPaidOn;

    /**
     * @Groups({"flat:collection"})
     */
    private ?DateTimeInterface $insuredUntil;

    /**
     * @Groups({"flat:collection"})
     */
    private ?float $rentPrice;

    /**
     * @Groups({"flat:item"})
     */
    private ?DateTimeInterface $purchaseDate;

    /**
     * @Groups({"flat:item"})
     */
    private ?DateTimeInterface $tenantExpiredAt;

    /**
     * @Groups({"flat:collection"})
     */
    private ?AddressModel $address;

    /**
     * @Groups({"flat:collection"})
     */
    private ?FlatPictureModel $mainPicture;

    /**
     * @Groups({"flat:item"})
     */
    private ?TenantModel $tenant;

    /**
     * @Groups({"flat:item"})
     */
    private ?RentalDetailsModel $rentalDetails;

    /**
     * @var TenantPaymentModel[]|Collection
     * @Groups({"flat:item"})
     */
    private Collection $payments;

    /**
     * @var FlatPicture[]|Collection
     * @Groups({"flat:pictures"})
     */
    private Collection $pictures;

    /**
     * @Groups({"flat:item"})
     */
    private ?float $sumRent;

    public function __construct()
    {
        $this->payments = new ArrayCollection();
        $this->pictures = new ArrayCollection();
    }

    /**
     * @param Flat $flat
     */
    public function buildFromEntity(object $flat): FlatModel
    {
        $sumRent = 0;
        $model = $this
            ->setId($flat->getId())
            ->setFlatPrice($flat->getFlatPrice())
            ->setName($flat->getName())
            ->setNotaryFees($flat->getNotaryFees())
            ->setPurchaseDate($flat->getPurchaseDate())
            ->setRentedSince($flat->getRentedSince())
            ->setRentPrice($flat->getRentPrice())
            ->setInsuredUntil($flat->getInsuranceExpirationDate())
            ->setLastRentPaidOn($flat->getLastRentPaidOn())
            ->setTenantExpiredAt($flat->getActiveTenantFlat()?->getExpiredAt())
            ->setRentalDetails((new RentalDetailsModel())->buildFromEntity($flat->getRentalDetails()))
            ->setTenant((new TenantModel())->buildFromEntity($flat->getActiveTenant()))
            ->setAddress((new AddressModel())->buildFromEntity($flat->getAddress()))
            ->setMainPicture($flat->getPictures()->count() > 0 ?
                (new FlatPictureModel())->buildFromEntity($flat->getPictures()->first()) : null);

        foreach ($flat->getPayments() as $payment) {
            $this->addPayment((new TenantPaymentModel())->buildFromEntity($payment));
            if($payment->getReceiptSendAt()){
                $sumRent += $payment->getAmountPaid();
            }
        }

        $model->setSumRent($sumRent);

        foreach ($flat->getPictures() as $picture) {
            $this->addPicture((new FlatPictureModel())->buildFromEntity($picture));
        }

        return $model;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): FlatModel
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): FlatModel
    {
        $this->name = $name;

        return $this;
    }

    public function getFlatPrice(): ?float
    {
        return $this->flatPrice;
    }

    public function setFlatPrice(?float $flatPrice): FlatModel
    {
        $this->flatPrice = $flatPrice;

        return $this;
    }

    public function getNotaryFees(): ?float
    {
        return $this->notaryFees;
    }

    public function setNotaryFees(?float $notaryFees): FlatModel
    {
        $this->notaryFees = $notaryFees;

        return $this;
    }

    public function getPurchaseDate(): ?DateTimeInterface
    {
        return $this->purchaseDate;
    }

    public function setPurchaseDate(?DateTimeInterface $purchaseDate): FlatModel
    {
        $this->purchaseDate = $purchaseDate;

        return $this;
    }

    public function getAddress(): ?AddressModel
    {
        return $this->address;
    }

    public function setAddress(?AddressModel $address): FlatModel
    {
        $this->address = $address;

        return $this;
    }

    public function getRentedSince(): ?DateTimeInterface
    {
        return $this->rentedSince;
    }

    public function setRentedSince(?DateTimeInterface $rentedSince): FlatModel
    {
        $this->rentedSince = $rentedSince;

        return $this;
    }

    public function getLastRentPaidOn(): ?DateTimeInterface
    {
        return $this->lastRentPaidOn;
    }

    public function setLastRentPaidOn(?DateTimeInterface $lastRentPaidOn): FlatModel
    {
        $this->lastRentPaidOn = $lastRentPaidOn;

        return $this;
    }

    public function getInsuredUntil(): ?DateTimeInterface
    {
        return $this->insuredUntil;
    }

    public function setInsuredUntil(?DateTimeInterface $insuredUntil): FlatModel
    {
        $this->insuredUntil = $insuredUntil;

        return $this;
    }

    public function getRentPrice(): ?float
    {
        return $this->rentPrice;
    }

    public function setRentPrice(?float $rentPrice): FlatModel
    {
        $this->rentPrice = $rentPrice;

        return $this;
    }

    public function getTenant(): ?TenantModel
    {
        return $this->tenant;
    }

    public function setTenant(?TenantModel $tenant): FlatModel
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getRentalDetails(): ?RentalDetailsModel
    {
        return $this->rentalDetails;
    }

    public function setRentalDetails(?RentalDetailsModel $rentalDetails): FlatModel
    {
        $this->rentalDetails = $rentalDetails;

        return $this;
    }

    public function getSumRent(): ?float
    {
        return $this->sumRent;
    }

    public function setSumRent(?float $sumRent): FlatModel
    {
        $this->sumRent = $sumRent;

        return $this;
    }

    /**
     * @return TenantPaymentModel[]|Collection
     */
    public function getPayments(): Collection|array
    {
        return $this->payments;
    }

    public function addPayment(TenantPaymentModel $tenantPaymentModel): FlatModel
    {
        if (!$this->payments->contains($tenantPaymentModel)) {
            $this->payments->add($tenantPaymentModel);
        }

        return $this;
    }

    public function addPicture(FlatPictureModel $flatPictureModel): FlatModel
    {
        if (!$this->pictures->contains($flatPictureModel)) {
            $this->pictures->add($flatPictureModel);
        }

        return $this;
    }

    /**
     * @return FlatPicture[]|Collection
     */
    public function getPictures(): Collection|array
    {
        return $this->pictures;
    }

    public function getMainPicture(): ?FlatPictureModel
    {
        return $this->mainPicture;
    }

    public function setMainPicture(?FlatPictureModel $mainPicture): FlatModel
    {
        $this->mainPicture = $mainPicture;

        return $this;
    }

    public function getTenantExpiredAt(): ?DateTimeInterface
    {
        return $this->tenantExpiredAt;
    }

    public function setTenantExpiredAt(?DateTimeInterface $tenantExpiredAt): FlatModel
    {
        $this->tenantExpiredAt = $tenantExpiredAt;

        return $this;
    }
}
