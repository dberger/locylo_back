<?php

namespace App\Model;

interface BaseModelInterface
{
    public function buildFromEntity(object $object): self;
}
