<?php

namespace App\Model\Search;

class FlatSearch
{
    private ?int $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): FlatSearch
    {
        $this->id = $id;

        return $this;
    }
}
