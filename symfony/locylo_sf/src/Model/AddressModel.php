<?php

namespace App\Model;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AddressModel.
 */
class AddressModel implements BaseModelInterface
{
    /**
     * @Groups({"flat:collection"})
     */
    private ?int $id;

    /**
     * @var string|null
     * @Groups({"flat:collection"})
     */
    #[Assert\NotBlank]
    private ?string $address;

    /**
     * @var string|null
     * @Groups({"flat:collection"})
     */
    #[Assert\NotBlank]
    private ?string $zip;

    /**
     * @var string|null
     * @Groups({"flat:collection"})
     */
    #[Assert\NotBlank]
    private ?string $city;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function buildFromEntity(object $address): self
    {
        return $this
            ->setId($address->getId())
            ->setCity($address->getCity())
            ->setAddress($address->getAddress())
            ->setZip($address->getZip())
        ;
    }

    public function setId(?int $id): AddressModel
    {
        $this->id = $id;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): AddressModel
    {
        $this->address = $address;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): AddressModel
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): AddressModel
    {
        $this->city = $city;

        return $this;
    }
}
