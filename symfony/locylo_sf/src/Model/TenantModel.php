<?php

namespace App\Model;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Tenant;
use DateTimeInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class TenantModel.
 */
#[ApiResource(
    collectionOperations: [
        'post' => [
            'validation_groups' => ['Default', 'tenant_add'],
            'normalizationContext' => ['groups' => ['tenant:add']],
            'denormalizationContext' => ['groups' => ['tenant:add']],
        ],
    ],
    iri: 'http://schema.org/TenantModel',
    itemOperations: [
        'patch' => [
            'validation_groups' => ['Default', 'tenant_edit'],
            'normalizationContext' => ['groups' => ['tenant:edit']],
            'denormalizationContext' => ['groups' => ['tenant:edit']],
        ],
        'get' => [
            'normalizationContext' => ['groups' => ['flat:item']],
            'denormalizationContext' => ['groups' => ['flat:item']],
        ],
    ]
)]
class TenantModel implements BaseModelInterface
{
    #[Groups(['flat:item'])]
    private ?int $id = null;

    #[Groups(['tenant:edit', 'flat:item', 'tenant:add'])]
    #[Assert\NotBlank]
    private ?string $lastname;

    #[Groups(['tenant:edit', 'flat:item', 'tenant:add'])]
    private ?string $firstname;

    #[Groups(['tenant:edit', 'flat:item', 'tenant:add'])]
    private ?string $phone;

    #[Groups(['tenant:edit', 'flat:item', 'tenant:add'])]
    private ?string $mail;

    #[Groups(['tenant:edit'])]
    private ?DateTimeInterface $expiredAt = null;

    #[Groups(['tenant:add'])]
    #[Assert\NotBlank(groups: ['tenant_add'])]
    private ?DateTimeInterface $startAt = null;

    #[Assert\NotBlank(groups: ['tenant:add', 'tenant_edit', 'tenant_add'])]
    #[Groups(['tenant:edit'])]
    private ?int $flatId = null;

    /**
     * @param Tenant $tenant
     *
     * @return TenantModel
     */
    public function buildFromEntity(object $tenant): self
    {
        return $this
            ->setId($tenant->getId())
            ->setFirstname($tenant->getFirstname())
            ->setLastname($tenant->getLastname())
            ->setMail($tenant->getMail())
            ->setPhone($tenant->getPhone());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): TenantModel
    {
        $this->id = $id;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): TenantModel
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): TenantModel
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): TenantModel
    {
        $this->phone = $phone;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(?string $mail): TenantModel
    {
        $this->mail = $mail;

        return $this;
    }

    public function getExpiredAt(): ?DateTimeInterface
    {
        return $this->expiredAt;
    }

    public function setExpiredAt(?DateTimeInterface $expiredAt): TenantModel
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    public function getFlatId(): ?int
    {
        return $this->flatId;
    }

    public function setFlatId(?int $flatId): TenantModel
    {
        $this->flatId = $flatId;

        return $this;
    }

    public function getStartAt(): ?DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(?DateTimeInterface $startAt): TenantModel
    {
        $this->startAt = $startAt;

        return $this;
    }
}
