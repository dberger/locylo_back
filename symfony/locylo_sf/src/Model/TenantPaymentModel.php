<?php

namespace App\Model;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\TenantPayment;
use DateTimeInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class TenantPaymentModel.
 */
#[ApiResource(
    collectionOperations: ['post' => [
        'normalization_context' => ['groups' => ['tenantPayment:read']],
        'denormalization_context' => ['groups' => ['tenantPayment:write']],
    ]],
    itemOperations: [
        'get' => ['normalization_context' => ['groups' => ['tenantPayment:read']]],
        'patch' => ['denormalization_context' => ['groups' => ['tenantPayment:write']]],
    ]
)]
class TenantPaymentModel implements BaseModelInterface
{
    /**
     * @Groups({"tenantPayment:read", "tenantPayment:write", "flat:item"})
     */
    private ?int $id = null;

    /**
     * @Groups({"tenantPayment:read", "tenantPayment:write", "flat:item"})
     */
    #[Assert\NotBlank]
    private ?DateTimeInterface $paymentDate = null;

    /**
     * @Groups({"tenantPayment:read", "tenantPayment:write", "flat:item"})
     */
    private ?DateTimeInterface $receiptSendAt = null;

    /**
     * @Groups({"tenantPayment:read", "tenantPayment:write", "flat:item"})
     */
    #[Assert\NotBlank]
    private ?float $amountPaid = null;

    /**
     * @Groups({"tenantPayment:read", "tenantPayment:write", "flat:item"})
     */
    #[Assert\NotBlank]
    private ?DateTimeInterface $debtDate = null;

    /**
     * @Groups({"tenantPayment:read", "tenantPayment:write", "flat:item"})
     */
    #[Assert\NotBlank]
    private string $flatId;

    /**
     * @param TenantPayment $tenantPayment
     *
     * @return $this
     */
    public function buildFromEntity(object $tenantPayment): self
    {
        return $this
            ->setId($tenantPayment->getId())
            ->setAmountPaid($tenantPayment->getAmountPaid())
            ->setPaymentDate($tenantPayment->getPaymentDate())
            ->setReceiptSendAt($tenantPayment->getReceiptSendAt())
            ->setDebtDate($tenantPayment->getDebtDate())
            ->setFlatId($tenantPayment->getFlat()->getId())
            ;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): TenantPaymentModel
    {
        $this->id = $id;

        return $this;
    }

    public function getPaymentDate(): ?DateTimeInterface
    {
        return $this->paymentDate;
    }

    public function setPaymentDate(?DateTimeInterface $paymentDate): TenantPaymentModel
    {
        $this->paymentDate = $paymentDate;

        return $this;
    }

    public function getReceiptSendAt(): ?DateTimeInterface
    {
        return $this->receiptSendAt;
    }

    public function setReceiptSendAt(?DateTimeInterface $receiptSendAt): TenantPaymentModel
    {
        $this->receiptSendAt = $receiptSendAt;

        return $this;
    }

    public function getAmountPaid(): ?float
    {
        return $this->amountPaid;
    }

    public function setAmountPaid(?float $amountPaid): TenantPaymentModel
    {
        $this->amountPaid = $amountPaid;

        return $this;
    }

    public function getDebtDate(): ?DateTimeInterface
    {
        return $this->debtDate;
    }

    public function setDebtDate(?DateTimeInterface $debtDate): TenantPaymentModel
    {
        $this->debtDate = $debtDate;

        return $this;
    }

    public function getFlatId(): string
    {
        return $this->flatId;
    }

    public function setFlatId(string $flatId): TenantPaymentModel
    {
        $this->flatId = $flatId;

        return $this;
    }
}
