<?php

namespace App\Repository;

use App\Entity\Tenant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class TenantRepository.
 */
final class TenantRepository extends ServiceEntityRepository
{
    /**
     * TenantRepository constructor.
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tenant::class);
    }

    /**
     * @throws ORMException
     */
    public function persist(Tenant $tenant): Tenant
    {
        $this->_em->persist($tenant);

        return $tenant;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function flush(): void
    {
        $this->_em->flush();
    }
}
