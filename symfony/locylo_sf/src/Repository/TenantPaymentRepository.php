<?php

namespace App\Repository;

use App\Entity\TenantPayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class TenantPaymentRepository.
 */
final class TenantPaymentRepository extends ServiceEntityRepository
{
    /**
     * TenantPaymentRepository constructor.
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenantPayment::class);
    }

    /**
     * @throws ORMException
     */
    public function persist(TenantPayment $tenantPayment): TenantPayment
    {
        $this->_em->persist($tenantPayment);

        return $tenantPayment;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

    public function getOriginalData(TenantPayment $tenantPayment): array
    {
        return $this->_em->getUnitOfWork()->getOriginalEntityData($tenantPayment);
    }
}
