<?php

namespace App\Repository;

use App\Entity\TenantFlat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class TenantFlatRepository.
 */
final class TenantFlatRepository extends ServiceEntityRepository
{
    /**
     * TenantFlatRepository constructor.
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenantFlat::class);
    }

    /**
     * @throws ORMException
     */
    public function persist(TenantFlat $tenantFlat): TenantFlat
    {
        $this->_em->persist($tenantFlat);

        return $tenantFlat;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

    public function findOneByTenantIdAndFlatId(int $flatId, int $tenantId): ?TenantFlat
    {
        return $this->findOneBy([
            'flat' => $flatId,
            'tenant' => $tenantId,
        ]);
    }
}
