<?php

namespace App\Repository;

use App\Entity\Flat;
use App\Model\Search\FlatSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class FlatRepository.
 */
final class FlatRepository extends ServiceEntityRepository
{
    /**
     * FlatRepository constructor.
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Flat::class);
    }

    public function getSearchQuery(?FlatSearch $flatSearch = null): Query
    {
        $qb = $this
            ->createQueryBuilder('f');

        $qb->addOrderBy('f.name', 'asc');

        return $qb->getQuery();
    }
}
