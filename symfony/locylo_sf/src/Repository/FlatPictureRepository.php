<?php

namespace App\Repository;

use App\Entity\FlatPicture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class FlatPictureRepository.
 */
final class FlatPictureRepository extends ServiceEntityRepository
{
    /**
     * FlatPictureRepository constructor.
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FlatPicture::class);
    }

    /**
     * @throws ORMException
     */
    public function persist(FlatPicture $flatPicture): FlatPicture
    {
        $this->_em->persist($flatPicture);

        return $flatPicture;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

    public function remove(FlatPicture $picture)
    {
        $this->_em->remove($picture);
    }
}
