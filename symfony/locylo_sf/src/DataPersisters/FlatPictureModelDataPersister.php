<?php

namespace App\DataPersisters;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Model\FlatPictureModel;
use App\Services\FlatPicture\FlatPictureCrudManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class FlatPictureModelDataPersister.
 */
final class FlatPictureModelDataPersister implements ContextAwareDataPersisterInterface
{
    private FlatPictureCrudManager $flatPictureCrudManager;

    public function __construct(FlatPictureCrudManager $flatPictureCrudManager)
    {
        $this->flatPictureCrudManager = $flatPictureCrudManager;
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof FlatPictureModel;
    }

    /**
     * @param $data
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function persist($data, array $context = []): FlatPictureModel
    {
        return $this->flatPictureCrudManager->saveModel($data);
    }

    public function remove($data, array $context = []): FlatPictureModel
    {
        return $this->flatPictureCrudManager->removeModel($data);
    }
}
