<?php

namespace App\DataPersisters;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Model\TenantPaymentModel;
use App\Services\Payments\PaymentManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class TenantPaymentModelDataPersister.
 */
final class TenantPaymentModelDataPersister implements ContextAwareDataPersisterInterface
{
    private PaymentManager $paymentManager;

    public function __construct(PaymentManager $paymentManager)
    {
        $this->paymentManager = $paymentManager;
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof TenantPaymentModel;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function persist($data, array $context = [])
    {
        return $this->paymentManager->saveModel($data);
    }

    public function remove($data, array $context = [])
    {
        // call your persistence layer to delete $data
    }
}
