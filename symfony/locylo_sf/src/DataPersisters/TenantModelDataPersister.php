<?php

namespace App\DataPersisters;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Tenant;
use App\Entity\TenantFlat;
use App\Model\TenantModel;
use App\Repository\TenantFlatRepository;
use App\Repository\TenantRepository;
use App\Services\Flat\FlatSearchProvider;
use App\Services\Tenant\TenantSearchProvider;
use Doctrine\ORM\ORMException;
use InvalidArgumentException;
use LogicException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class TenantModelDataPersister.
 */
final class TenantModelDataPersister implements ContextAwareDataPersisterInterface
{
    private TenantSearchProvider $searchProvider;
    private TenantRepository $tenantRepository;
    private TenantFlatRepository $tenantFlatRepository;
    private FlatSearchProvider $flatSearchProvider;

    // TODO refacto découper sous service
    public function __construct(
        TenantSearchProvider $searchProvider,
        TenantRepository $tenantRepository,
        TenantFlatRepository $tenantFlatRepository,
        FlatSearchProvider $flatSearchProvider
    ) {
        $this->searchProvider = $searchProvider;
        $this->tenantRepository = $tenantRepository;
        $this->tenantFlatRepository = $tenantFlatRepository;
        $this->flatSearchProvider = $flatSearchProvider;
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof TenantModel;
    }

    /**
     * @param TenantModel $data
     *
     * @throws ORMException
     */
    public function persist($data, array $context = []): TenantModel
    {
        if ('post' === $context['collection_operation_name']) {
            return $this->handlePostPersist($data);
        }

        /** @var TenantModel $previousData */
        $previousData = $context['previous_data'];
        $tenant = $this->searchProvider->find($previousData->getId());

        if (!$tenant) {
            throw new NotFoundHttpException();
        }
        if ($data->getId() !== $previousData->getId()) {
            throw new InvalidArgumentException();
        }

        if (null !== $data->getExpiredAt() && $previousData->getExpiredAt() !== $data->getExpiredAt()) {
            $tenantFlat = $this->tenantFlatRepository
                ->findOneByTenantIdAndFlatId($data->getFlatId(), $tenant->getId());
            if (!$tenantFlat) {
                throw new LogicException('Impossible de changer la date d\'expiration');
            }
            $tenantFlat->setExpiredAt($data->getExpiredAt());
            $this->tenantFlatRepository->persist($tenantFlat);
        }

        $this->tenantRepository->persist($tenant->createFromModel($data));
        $this->tenantRepository->flush();

        return $data;
    }

    public function remove($data, array $context = []): TenantModel
    {
        // todo
    }

    private function handlePostPersist(TenantModel $data): TenantModel
    {
        $flat = $this->flatSearchProvider->find($data->getFlatId());
        if (null === $flat) {
            throw new NotFoundHttpException();
        }

        $currentActiveTenant = $flat->getActiveTenantFlat();
        // Si on a déjà un locataire alors on lui fait expirer son bail au début du nouveau locataire
        if ($currentActiveTenant instanceof TenantFlat) {
            $currentActiveTenant->setExpiredAt($data->getStartAt());
        }

        $newExpirationData = clone $data->getStartAt();
        $data->setExpiredAt($newExpirationData->modify('+3 year'));
        $tenant = (new Tenant())->createFromModel($data);
        $this->tenantRepository->persist($tenant);

        $tenantFlat = (new TenantFlat())
            ->setFlat($flat)
            ->setTenant($tenant)
            ->setStartAt($data->getStartAt())
            ->setExpiredAt($data->getExpiredAt());
        $this->tenantFlatRepository->persist($currentActiveTenant);
        $this->tenantFlatRepository->persist($tenantFlat);

        $this->tenantRepository->flush();

        return $data->setId($tenant->getId());
    }
}
