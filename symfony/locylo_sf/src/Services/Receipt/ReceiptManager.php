<?php

namespace App\Services\Receipt;

use App\Entity\TenantPayment;
use App\Model\Document\ReceiptDocumentModel;
use Knp\Snappy\Pdf;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;

final class ReceiptManager
{
    private string $projectDir;
    private Environment $environment;
    private Pdf $pdf;
    private MailerInterface $mailer;
    private string $ownerName;
    private string $ownerAddress;

    public function __construct(string $projectDir,
                                Environment $environment,
                                Pdf $pdf,
                                MailerInterface $mailer,
                                string $ownerName,
                                string $ownerAddress
    ) {
        $this->projectDir = $projectDir;
        $this->environment = $environment;
        $this->pdf = $pdf;
        $this->mailer = $mailer;
        $this->ownerName = $ownerName;
        $this->ownerAddress = $ownerAddress;
    }

    public function sendReceipt(TenantPayment $tenantPayment): bool
    {
        // TODO voir si on remet du docx
        $modelPath = $this->projectDir.ReceiptDocumentModel::MODEL_PATH;
        $outputPath = str_replace('{flatId}',
            $tenantPayment->getFlat()->getId(),
            $this->projectDir.ReceiptDocumentModel::OUTPUT_PATH);

        if (!file_exists($outputPath)) {
            mkdir($outputPath, 755);
        }

        $fileNamePdf = sprintf('receipt_%s.pdf', date('dmyhis'));

        $html = $this->environment->render('receipt/model.html.twig', [
            'now' => $tenantPayment->getReceiptSendAt(),
            'period_start' => $tenantPayment->getDebtDate()->format('d/m/Y'),
            'period_end' => (clone $tenantPayment->getDebtDate())->modify('last day of')->format('d/m/Y'),
            'firstName' => $tenantPayment->getTenant()->getFirstname(),
            'lastName' => $tenantPayment->getTenant()->getLastname(),
            'address' => $tenantPayment->getFlat()->getAddress()->getAddress(),
            'city' => $tenantPayment->getFlat()->getAddress()->getCity(),
            'zip' => $tenantPayment->getFlat()->getAddress()->getZip(),
            'price' => $tenantPayment->getFlat()->getRentalDetails()->getPrice(),
            'charges' => $tenantPayment->getFlat()->getRentalDetails()->getChargesAmount(),
            'total' => $tenantPayment->getFlat()->getRentalDetails()->getTotalPrice(),
            'paymentDate' => $tenantPayment->getPaymentDate(),
            'ownerName' => $this->ownerName,
            'ownerAddress' => $this->ownerAddress,
        ]);

        $this->pdf->generateFromHtml($html, $outputPath.'/'.$fileNamePdf, [
            'zoom' => 1,
            'margin-top' => '15mm',
            'margin-bottom' => '15mm',
            'margin-left' => '37mm',
            'header-spacing' => '10',
            'footer-spacing' => '10',
        ]);

        $message = (new Email())
            ->subject(sprintf('Quittance de loyer %s',
                $tenantPayment->getDebtDate()->format('m-Y')))
            ->from('dimitri@dimitri-berger.fr')
            ->to($tenantPayment->getTenant()->getMail())
            ->bcc('dimitri@dimitri-berger.fr')
            ->html(
                $this->environment->render(
                    'receipt/mail.html.twig',
                    [
                        'period_start' => $tenantPayment->getDebtDate()->format('d/m/Y'),
                        'period_end' => (clone $tenantPayment->getDebtDate())->modify('last day of')->format('d/m/Y'),
                        'ownerName' => $this->ownerName,
                    ]
                ),
                'text/html'
            )
            ->attachFromPath($outputPath.'/'.$fileNamePdf)
        ;

        $this->mailer->send($message);

        return true;
    }

    private function createValues(TenantPayment $tenantPayment): array
    {
        return [
            'BERGER Dimitri',
            '20 rue des gourlettes / Clermont - Ferrand / 63000',
            $tenantPayment->getReceiptSendAt()->format('d/m/Y'),
            $tenantPayment->getDebtDate()->format('m-Y'),
            $tenantPayment->getTenant()->getFirstname().' '.$tenantPayment->getTenant()->getLastname(),
            sprintf('%s / %s / %s',
                $tenantPayment->getFlat()->getAddress()->getAddress(),
                $tenantPayment->getFlat()->getAddress()->getCity(),
                $tenantPayment->getFlat()->getAddress()->getZip()
            ),
            $tenantPayment->getFlat()->getRentalDetails()->getPrice(),
            $tenantPayment->getFlat()->getRentalDetails()->getChargesAmount(),
            $tenantPayment->getFlat()->getRentalDetails()->getTotalPrice(),
            $tenantPayment->getPaymentDate()->format('d/m/Y'),
            sprintf('%s / %s / %s',
                $tenantPayment->getFlat()->getAddress()->getAddress(),
                $tenantPayment->getFlat()->getAddress()->getCity(),
                $tenantPayment->getFlat()->getAddress()->getZip()
            ),
            $tenantPayment->getAmountPaid(),
        ];
    }
}
