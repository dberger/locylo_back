<?php

namespace App\Services\Payments;

use App\Entity\Flat;
use App\Entity\TenantPayment;
use App\Event\PaymentReceiptDateEvent;
use App\Model\TenantPaymentModel;
use App\Repository\TenantPaymentRepository;
use App\Services\Flat\FlatSearchProvider;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use LogicException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class PaymentManager
{
    private FlatSearchProvider $flatSearchProvider;
    private TenantPaymentRepository $paymentRepository;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(FlatSearchProvider $flatSearchProvider,
                                TenantPaymentRepository $paymentRepository,
                                EventDispatcherInterface $eventDispatcher
    ) {
        $this->flatSearchProvider = $flatSearchProvider;
        $this->paymentRepository = $paymentRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function saveModel(TenantPaymentModel $tenantPaymentModel): TenantPaymentModel
    {
        $flat = $this->flatSearchProvider->find($tenantPaymentModel->getFlatId());
        $payment = null;

        if (empty($tenantPaymentModel->getFlatId()) || !$flat instanceof Flat) {
            throw new LogicException('Can\'t link the payment to an existing flat !');
        }

        if (empty($flat->getActiveTenant())) {
            throw new LogicException('There is no active tenant in this flat !');
        }

        if (!empty($tenantPaymentModel->getId())) {
            $payment = $this->paymentRepository->find($tenantPaymentModel->getId());
        }

        if (empty($payment)) {
            $payment = new TenantPayment();
        }

        $payment
            ->createFromModel($tenantPaymentModel)
            ->setFlat($flat)
            ->setTenant($flat->getActiveTenant())
        ;

        $originalData = $this->paymentRepository->getOriginalData($payment);

        if (is_array($originalData) && empty($originalData['receiptSendAt']) && !empty($payment->getReceiptSendAt())) {
            $this->eventDispatcher
                ->dispatch(new PaymentReceiptDateEvent($payment), PaymentReceiptDateEvent::NAME);
        }

        $this->paymentRepository->persist($payment);
        $this->paymentRepository->flush();

        $tenantPaymentModel->setId($payment->getId());

        return $tenantPaymentModel;
    }
}
