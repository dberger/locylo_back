<?php

namespace App\Services\Payments;

use App\DataPersisters\TenantPaymentModelDataPersister;
use App\Entity\Flat;
use App\Model\FlatModel;
use App\Model\TenantPaymentModel;
use DateTime;
use RRule\RRule;

final class FlatPaymentsBookManager
{
    private TenantPaymentModelDataPersister $dataPersister;

    public function __construct(TenantPaymentModelDataPersister $dataPersister)
    {
        $this->dataPersister = $dataPersister;
    }

    public function addMissingPaymentsInModel(Flat $flat, FlatModel $model): FlatModel
    {
        $dateMonthPlus = (new DateTime())->modify('+1 month');
        $lastCreatedPaymentDebtDate = $flat->getLastCreatedPayment()->getDebtDate();
        $lastCreatedPaymentDateFormat = $lastCreatedPaymentDebtDate->format('Y-m-d');
        $rrule = new RRule([
            'FREQ' => 'MONTHLY',
            'INTERVAL' => 1,
            'DTSTART' => $lastCreatedPaymentDateFormat,
            'COUNT' => 50,
        ]);

        foreach ($rrule as $key => $recurrence) {
            if (0 === strpos($lastCreatedPaymentDateFormat, $recurrence->format('Y-m-d'))) {
                continue;
            }
            // Si la recurrence est de plus d'un mois à partir de maintenant
            if ($dateMonthPlus < $recurrence) {
                continue;
            }
            $currentRecurrence = $recurrence->setTime(0, 0, 0);

            $clone = clone $currentRecurrence;
            $tenantPaymentModel = (new TenantPaymentModel())
                ->setDebtDate($clone)
                ->setAmountPaid($flat->getRentalDetails()->getTotalPrice())
                ->setFlatId($flat->getId());
            $this->dataPersister->persist($tenantPaymentModel);
            $model->addPayment(
                $tenantPaymentModel
            );
        }

        return $model;
    }
}
