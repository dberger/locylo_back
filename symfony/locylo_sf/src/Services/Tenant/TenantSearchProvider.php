<?php

namespace App\Services\Tenant;

use App\Entity\Tenant;
use App\Repository\TenantRepository;

/**
 * Class TenantSearchProvider.
 */
final class TenantSearchProvider
{
    private TenantRepository $tenantRepository;

    /**
     * TenantSearchProvider constructor.
     */
    public function __construct(TenantRepository $tenantRepository)
    {
        $this->tenantRepository = $tenantRepository;
    }

    /**
     * @return Tenant[]|array
     */
    public function findAll(): array
    {
        return $this->tenantRepository->findAll();
    }

    public function find(int $id): ?Tenant
    {
        return $this->tenantRepository->find($id);
    }
}
