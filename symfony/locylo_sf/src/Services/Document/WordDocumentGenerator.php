<?php

namespace App\Services\Document;

use App\PhpOffice\CustomTemplateProcessor;
use PhpOffice\PhpWord\Exception\CopyFileException;
use PhpOffice\PhpWord\Exception\CreateTemporaryFileException;

final class WordDocumentGenerator
{
    /**
     * @throws CopyFileException
     * @throws CreateTemporaryFileException
     */
    public function generateAndReplaceValues(
        string $modelPath,
        string $exportPath,
        array $valuesToReplace,
        array $realValues
    ): ?string {
        if (!file_exists($modelPath)) {
            return null;
        }

        foreach ($realValues as $i => $realValue) {
            $realValues[$i] = str_replace('&', 'et', $realValue);
        }

        $templateProcessor = new CustomTemplateProcessor($modelPath);
        $templateProcessor->setValue($valuesToReplace, $realValues);
        $templateProcessor->saveAs($exportPath);

        return $exportPath;
    }
}
