<?php

namespace App\Services\FlatPicture;

use App\Entity\Flat;
use App\Entity\FlatPicture;
use App\Model\FlatPictureModel;
use App\Repository\FlatPictureRepository;
use App\Services\Flat\FlatSearchProvider;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use LogicException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class FlatPictureCrudManager
{
    private FlatSearchProvider $flatSearchProvider;

    private FlatPictureRepository $flatPictureRepository;

    public function __construct(FlatSearchProvider $flatSearchProvider, FlatPictureRepository $flatPictureRepository)
    {
        $this->flatSearchProvider = $flatSearchProvider;
        $this->flatPictureRepository = $flatPictureRepository;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function saveModel(FlatPictureModel $flatPictureModel): FlatPictureModel
    {
        $flat = $this->flatSearchProvider->find($flatPictureModel->getFlatId());
        if (empty($flatPictureModel->getFlatId()) || !$flat instanceof Flat) {
            throw new LogicException('Can\'t link the picture to an existing flat !');
        }

        $picture = (new FlatPicture())
            ->createFromModel($flatPictureModel)
            ->setFlat($flat);

        $this->flatPictureRepository->persist($picture);
        $this->flatPictureRepository->flush();

        return $flatPictureModel
            ->setId($picture->getId())
            ->setFile($picture->getFile())
            ->setFilePath($picture->getFilePath())
            ;
    }

    public function removeModel(FlatPictureModel $flatPictureModel): FlatPictureModel
    {
        $picture = $this->flatPictureRepository->find($flatPictureModel->getId());
        if (!$picture instanceof FlatPicture) {
            throw new NotFoundHttpException('This picture does not exists !');
        }

        $this->flatPictureRepository->remove($picture);
        $this->flatPictureRepository->flush();

        return $flatPictureModel;
    }
}
