<?php

namespace App\Services\Flat;

use App\Entity\TenantPayment;
use App\Repository\TenantPaymentRepository;

/**
 * Class TenantPaymentSearchProvider.
 */
final class TenantPaymentSearchProvider
{
    private TenantPaymentRepository $tenantPaymentRepository;

    /**
     * TenantPaymentSearchProvider constructor.
     */
    public function __construct(TenantPaymentRepository $tenantPaymentRepository)
    {
        $this->tenantPaymentRepository = $tenantPaymentRepository;
    }

    /**
     * @return TenantPayment[]|array
     */
    public function findAll(): array
    {
        return $this->tenantPaymentRepository->findAll();
    }

    public function find(int $id): ?TenantPayment
    {
        return $this->tenantPaymentRepository->find($id);
    }
}
