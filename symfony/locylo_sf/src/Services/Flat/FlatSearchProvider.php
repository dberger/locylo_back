<?php

namespace App\Services\Flat;

use App\Entity\Flat;
use App\Repository\FlatRepository;

/**
 * Class FlatSearchProvider.
 */
final class FlatSearchProvider
{
    private FlatRepository $flatRepository;

    /**
     * FlatSearchProvider constructor.
     */
    public function __construct(FlatRepository $flatRepository)
    {
        $this->flatRepository = $flatRepository;
    }

    /**
     * @return Flat[]|array
     */
    public function findAll(): array
    {
        return $this->flatRepository->getSearchQuery()->execute();
    }

    public function find(int $id): ?Flat
    {
        return $this->flatRepository->find($id);
    }
}
