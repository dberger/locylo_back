<?php

namespace App\Controller;

use App\DataPersisters\FlatPictureModelDataPersister;
use App\Model\FlatPictureModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

#[AsController]
final class CreateFlatPictureAction extends AbstractController
{
    public function __invoke(Request $request, FlatPictureModelDataPersister $flatPictureModelDataPersister): FlatPictureModel
    {
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        return (new FlatPictureModel())
            ->setFile($uploadedFile)
            ->setFlatId($request->get('flatId'))
        ;
    }
}
