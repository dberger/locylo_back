<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add table flat_picture
 */
final class Version20210819120850 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add table flat_picture';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE flat_picture (id INT AUTO_INCREMENT NOT NULL, flat_id INT NOT NULL COMMENT \'Flat id\', updated_at DATETIME DEFAULT NULL, file_path VARCHAR(255) DEFAULT NULL, INDEX IDX_72AF8772D3331C94 (flat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE flat_picture ADD CONSTRAINT FK_72AF8772D3331C94 FOREIGN KEY (flat_id) REFERENCES flat (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE flat_picture');
    }
}
