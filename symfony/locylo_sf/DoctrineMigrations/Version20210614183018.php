<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add debtdate column
 */
final class Version20210614183018 extends AbstractMigration
{
    /**
     * @return string
     */
    public function getDescription(): string
    {
        return 'Add debtdate column';
    }

    /**
     * @param Schema $schema
     * @return void
     */
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tenant_payment ADD debt_date DATETIME NOT NULL COMMENT \'Debt Date\', ADD created_at DATETIME NOT NULL COMMENT \'Created At\', ADD updated_at DATETIME NOT NULL COMMENT \'Updated At\'');
    }

    /**
     * @param Schema $schema
     * @return void
     */
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tenant_payment DROP debt_date, DROP created_at, DROP updated_at');
    }
}
