<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add nullable payment_date
 */
final class Version20210626070417 extends AbstractMigration
{
    /**
     * @return string
     */
    public function getDescription(): string
    {
        return 'Add nullable payment_date';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tenant_payment CHANGE payment_date payment_date DATETIME DEFAULT NULL COMMENT \'Payment received At\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tenant_payment CHANGE payment_date payment_date DATETIME NOT NULL COMMENT \'Payment received At\'');
    }
}
