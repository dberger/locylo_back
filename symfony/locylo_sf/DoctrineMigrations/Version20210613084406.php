<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Init structure
 */
final class Version20210613084406 extends AbstractMigration
{
    /**
     * @return string
     */
    public function getDescription(): string
    {
        return 'Init structure';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL COMMENT \'Address id\', flat_id INT DEFAULT NULL COMMENT \'Flat id\', address VARCHAR(255) NOT NULL COMMENT \'Address (street number/name)\', zip VARCHAR(255) NOT NULL COMMENT \'Zip code\', city VARCHAR(255) NOT NULL COMMENT \'City name\', lat VARCHAR(255) DEFAULT NULL COMMENT \'Latitude\', lng VARCHAR(255) DEFAULT NULL COMMENT \'Longitude\', UNIQUE INDEX UNIQ_D4E6F81D3331C94 (flat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'Table of Addresses\' ');
        $this->addSql('CREATE TABLE flat (id INT AUTO_INCREMENT NOT NULL COMMENT \'Flat id\', name VARCHAR(255) NOT NULL COMMENT \'Name of the flat\', flat_price NUMERIC(10, 2) DEFAULT NULL COMMENT \'Flat Price\', notary_fees NUMERIC(10, 2) DEFAULT NULL COMMENT \'Notary fees\', purchase_date DATETIME DEFAULT NULL COMMENT \'Purchase Date\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'Table of Flat\' ');
        $this->addSql('CREATE TABLE insurance (id INT AUTO_INCREMENT NOT NULL COMMENT \'Insurance id\', flat_id INT NOT NULL COMMENT \'Flat id\', tenant_id INT NOT NULL COMMENT \'Tenant id\', start_at DATETIME NOT NULL COMMENT \'Insurance start at\', expired_at DATETIME NOT NULL COMMENT \'Insurance end at\', company VARCHAR(255) NOT NULL COMMENT \'Company name\', active TINYINT(1) NOT NULL COMMENT \'Is active\', INDEX IDX_640EAF4CD3331C94 (flat_id), INDEX IDX_640EAF4C9033212A (tenant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'Table of Insurances\' ');
        $this->addSql('CREATE TABLE rental_details (id INT AUTO_INCREMENT NOT NULL COMMENT \'Rental details id\', flat_id INT DEFAULT NULL COMMENT \'Flat id\', price NUMERIC(10, 2) NOT NULL COMMENT \'Price without charges\', charges_amount NUMERIC(10, 2) NOT NULL COMMENT \'Charges amount\', created_at DATETIME NOT NULL COMMENT \'Creation At\', updated_at DATETIME NOT NULL COMMENT \'Updated At\', UNIQUE INDEX UNIQ_A1C0D165D3331C94 (flat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'Table of Rental details\' ');
        $this->addSql('CREATE TABLE tenant (id INT AUTO_INCREMENT NOT NULL COMMENT \'Tenant id\', lastname VARCHAR(255) NOT NULL COMMENT \'Last name\', firstname VARCHAR(255) DEFAULT NULL COMMENT \'First name\', phone VARCHAR(255) DEFAULT NULL COMMENT \'Phone number\', mail VARCHAR(255) DEFAULT NULL COMMENT \'Mail address\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'Table of Tenants\' ');
        $this->addSql('CREATE TABLE tenant_flat (flat_id INT NOT NULL COMMENT \'Flat id\', tenant_id INT NOT NULL COMMENT \'Tenant id\', start_at DATETIME NOT NULL COMMENT \'Start of the lease\', expired_at DATETIME NOT NULL COMMENT \'End of the lease\', INDEX IDX_1F25F097D3331C94 (flat_id), INDEX IDX_1F25F0979033212A (tenant_id), PRIMARY KEY(flat_id, tenant_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'Tenant-Flat join table\' ');
        $this->addSql('CREATE TABLE tenant_payment (id INT AUTO_INCREMENT NOT NULL COMMENT \'Payment id\', flat_id INT NOT NULL COMMENT \'Flat id\', tenant_id INT NOT NULL COMMENT \'Tenant id\', payment_date DATETIME NOT NULL COMMENT \'Payment received At\', receipt_send_at DATETIME DEFAULT NULL COMMENT \'Receipt send at\', amount_paid NUMERIC(10, 2) NOT NULL COMMENT \'Amount Paid\', INDEX IDX_7FBEC639D3331C94 (flat_id), INDEX IDX_7FBEC6399033212A (tenant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'Payments table\' ');
        $this->addSql('ALTER TABLE address ADD CONSTRAINT FK_D4E6F81D3331C94 FOREIGN KEY (flat_id) REFERENCES flat (id)');
        $this->addSql('ALTER TABLE insurance ADD CONSTRAINT FK_640EAF4CD3331C94 FOREIGN KEY (flat_id) REFERENCES flat (id)');
        $this->addSql('ALTER TABLE insurance ADD CONSTRAINT FK_640EAF4C9033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id)');
        $this->addSql('ALTER TABLE rental_details ADD CONSTRAINT FK_A1C0D165D3331C94 FOREIGN KEY (flat_id) REFERENCES flat (id)');
        $this->addSql('ALTER TABLE tenant_flat ADD CONSTRAINT FK_1F25F097D3331C94 FOREIGN KEY (flat_id) REFERENCES flat (id)');
        $this->addSql('ALTER TABLE tenant_flat ADD CONSTRAINT FK_1F25F0979033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id)');
        $this->addSql('ALTER TABLE tenant_payment ADD CONSTRAINT FK_7FBEC639D3331C94 FOREIGN KEY (flat_id) REFERENCES flat (id)');
        $this->addSql('ALTER TABLE tenant_payment ADD CONSTRAINT FK_7FBEC6399033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE address DROP FOREIGN KEY FK_D4E6F81D3331C94');
        $this->addSql('ALTER TABLE insurance DROP FOREIGN KEY FK_640EAF4CD3331C94');
        $this->addSql('ALTER TABLE rental_details DROP FOREIGN KEY FK_A1C0D165D3331C94');
        $this->addSql('ALTER TABLE tenant_flat DROP FOREIGN KEY FK_1F25F097D3331C94');
        $this->addSql('ALTER TABLE tenant_payment DROP FOREIGN KEY FK_7FBEC639D3331C94');
        $this->addSql('ALTER TABLE insurance DROP FOREIGN KEY FK_640EAF4C9033212A');
        $this->addSql('ALTER TABLE tenant_flat DROP FOREIGN KEY FK_1F25F0979033212A');
        $this->addSql('ALTER TABLE tenant_payment DROP FOREIGN KEY FK_7FBEC6399033212A');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE flat');
        $this->addSql('DROP TABLE insurance');
        $this->addSql('DROP TABLE rental_details');
        $this->addSql('DROP TABLE tenant');
        $this->addSql('DROP TABLE tenant_flat');
        $this->addSql('DROP TABLE tenant_payment');
    }
}
