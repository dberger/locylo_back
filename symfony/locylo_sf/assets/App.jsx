import React, { useState } from 'react'
import logo from './logo.svg'
import './App.css'
import {TracksList} from "./components/TracksList";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import {Track} from "./components/Track";
import {ErrorContext} from "./context/ErrorContext";

function App() {
  const [error, setError] = useState(null)

  return (
      <ErrorContext.Provider value={{error, setError}}>
        {error && <p>{error.toString()}</p>}
      <Router>
        <div>
          <Switch>
            <Route path="/track/:id">
              <Track/>
            </Route>
            <Route path="/">
              <TracksList />
            </Route>
          </Switch>
        </div>
      </Router>
      </ErrorContext.Provider>
  )
}

export default App
