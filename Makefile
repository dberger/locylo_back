up: 
	(cd infra && docker-compose up --build -d)
down: 
	docker-compose down
ssh:
	docker exec -it php-fpm bash
